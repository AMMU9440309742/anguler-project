import { Component } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  email: string = '';
  password: string = '';

  login() {
    // Log email and password to the console or perform login logic
    console.log('Email:', this.email);
    console.log('Password:', this.password);

    // Check login credentials
    if (this.email === "admin@ts" && this.password === "Admin@123") {
      alert("Login successful");
    } else {
      alert("Login failed");
    }
  }
}
