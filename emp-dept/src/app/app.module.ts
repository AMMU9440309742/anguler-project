import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ShowEmployeeComponent } from './show-employee/show-employee.component';
import { GenderPipe } from './gender.pipe';
import { ExperiencePipe } from './experience.pipe';
import { CountryPipe } from './country.pipe';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ShowEmployeeComponent,
    GenderPipe,
    ExperiencePipe,
    CountryPipe,
  
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
