
import { Component, OnInit } from '@angular/core';

export interface Employee {
  empId: number;
  empName: string;
  salary: number;
  gender: string;
  doj: string;
  age:number;
  country: string;
  emailId: string;
  password: string;
}

@Component({
  selector: 'app-show-employee',
  templateUrl: './show-employee.component.html',
  styleUrls: ['./show-employee.component.css'],
})
export class ShowEmployeeComponent implements OnInit {
  employees: Employee[] = [];
  displayedColumns: string[] = ['empId', 'empName', 'salary', 'gender', 'doj','age', 'country', 'emailId', 'password'];

  ngOnInit(): void {
    this.employees = [
      { empId: 101, empName: 'Vyshali', salary: 50000, gender: 'female', doj: '1990-01-15',age:77, country: 'India', emailId: 'john.doe@example.com', password: 'password123' },
      { empId: 102, empName: 'Riya', salary: 90000, gender: 'female', doj: '1966-05-22',age:33, country: 'United States', emailId: 'jane.doe@example.com', password: 'password456' },
      { empId: 103, empName: 'Sooraj', salary: 60000, gender: 'male', doj: '1980-05-22',age:90, country: 'Canada', emailId: 'sooraj@example.com', password: 'password789' },
      { empId: 104, empName: 'Yashna', salary: 60000, gender: 'female', doj: '1988-05-12',age:43, country: 'Australia', emailId: 'yashna@example.com', password: 'password000' },
    ];
  }
}
