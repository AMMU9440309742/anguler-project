// import { Pipe, PipeTransform } from '@angular/core';

// @Pipe({
//   name: 'experience'
// })
// export class ExperiencePipe implements PipeTransform {

//   transform(value: unknown, ...args: unknown[]): unknown {
//     return null;
//   }

// }
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'experience',
})
export class ExperiencePipe implements PipeTransform {
  transform(doj: string): string {
    const dojDate = new Date(doj);
    const currentDate = new Date();

    const experienceInMilliseconds = currentDate.getTime() - dojDate.getTime();
    const experienceInYears = Math.floor(experienceInMilliseconds / (365 * 24 * 60 * 60 * 1000));

    return `${experienceInYears} years`;
  }
}
